﻿using UnityEngine;

namespace game.scripts {
    public class CameraController : MonoBehaviour {

        public float panSpeed = 20f;
        public float panBorderThickness = 10f;
        public Vector2 panLimit = new Vector2(200, 100);
        public float scrollSpeed = 50f;
        public float minimumY = 20f;
        public float maximumY = 120f;

        // Update is called once per frame
        void Update() {
            Vector3 position = transform.position;

            // if (Input.GetKey("w") || Input.mousePosition.y > Screen.height - panBorderThickness) {
            // 	position.z += panSpeed * Time.deltaTime;
            // }
            // if (Input.GetKey("s") || Input.mousePosition.y < panBorderThickness) {
            // 	position.z -= panSpeed * Time.deltaTime;
            // }
            // if (Input.GetKey("d") || Input.mousePosition.x > Screen.width - panBorderThickness) {
            // 	position.x += panSpeed * Time.deltaTime;
            // }
            // if (Input.GetKey("a") || Input.mousePosition.x < panBorderThickness) {
            // 	position.x -= panSpeed * Time.deltaTime;
            // }

            if (Input.GetKey("w")) {
                position.z += panSpeed * Time.deltaTime;
            }
            if (Input.GetKey("s")) {
                position.z -= panSpeed * Time.deltaTime;
            }
            if (Input.GetKey("d")) {
                position.x += panSpeed * Time.deltaTime;
            }
            if (Input.GetKey("a")) {
                position.x -= panSpeed * Time.deltaTime;
            }
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            position.y += -scrollSpeed * 10f * scrollSpeed * Time.deltaTime;

            position.x = Mathf.Clamp(position.x, 0, panLimit.x);
            position.z = Mathf.Clamp(position.z, 0, panLimit.y);
            position.y = Mathf.Clamp(position.y, minimumY, maximumY);

            transform.position = position;
        }
    }
}