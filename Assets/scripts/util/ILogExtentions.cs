using System;
using log4net;
public static class ILogExtentions {
    public static void Trace(this ILog log, string message) {
        log.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, 
            log4net.Core.Level.Trace, message, null);
    }
}
