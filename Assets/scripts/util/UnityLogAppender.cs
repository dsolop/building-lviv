using log4net.Appender;
using log4net.Core;
using UnityEngine;

namespace game.scripts.util {
    public class UnityLogAppender : AppenderSkeleton {
        protected override void Append(LoggingEvent loggingEvent) {
            var message = RenderLoggingEvent(loggingEvent);
            if (loggingEvent.Level >= Level.Warn) {
                Debug.LogError(message);
            } else {
                Debug.Log(message);
            }
        }
    }
}