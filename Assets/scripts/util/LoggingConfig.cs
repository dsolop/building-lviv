using System.IO;
using log4net.Config;
using UnityEngine;

namespace game.scripts.util {
    public class LoggingConfig {

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void configure() {
            XmlConfigurator.Configure(new FileInfo(Application.dataPath + "/resources/log4net.xml"));
        }
        
    }
}