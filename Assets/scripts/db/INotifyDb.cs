﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace game.scripts.db {
    public sealed class INotifyDb {
        private static readonly INotifyDb instance = new INotifyDb();

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static INotifyDb() {
        }

        private INotifyDb() {
        }

        public static INotifyDb Instance {
            get {
                return instance;
            }
        }


        public Dictionary<string, KeyIndex<GuidIdVo>> keyIndexes = new Dictionary<string, KeyIndex<GuidIdVo>>();
        public Dictionary<Type, KeyIndex<GuidIdVo>> classToKeyIndex = new Dictionary<Type, KeyIndex<GuidIdVo>>();

        // todo generic
        public KeyIndex<GuidIdVo> createKeyIndex(String name, Type type) {
            KeyIndex<GuidIdVo> index = new KeyIndex<GuidIdVo>(name, type);
            keyIndexes.Add(name, index);
            classToKeyIndex.Add(type, index);
            return index;
        }

        public KeyIndex<GuidIdVo> getKeyIndex(String name) {
            KeyIndex<GuidIdVo> index;
            keyIndexes.TryGetValue(name, out index);
            return index;
        }

        public KeyIndex<GuidIdVo> getKeyIndex(Type type) {
            KeyIndex<GuidIdVo> index;
            classToKeyIndex.TryGetValue(type, out index);
            return index;
        }

        public void propertyChanged(object sender, PropertyChangedEventArgs e) {

        }

    }

}
