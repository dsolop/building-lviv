using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace game.scripts.db {

    [Serializable]
    public class GuidIdVo : INotifyPropertyChanged {

        public event PropertyChangedEventHandler PropertyChanged;

        public Guid id { get; private set; }
        public string name { get; set; }

        public GuidIdVo() {
            PropertyChanged += INotifyDb.Instance.propertyChanged;
            id = Guid.NewGuid();
        }

        public GuidIdVo(string name) : this() {
            this.name = name;
        }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType()) {
                return false;
            }
            return ((GuidIdVo)obj).id.Equals(this.id);
        }
        
        public override int GetHashCode() {
            return id.GetHashCode();
        }
    }
}