﻿using System;
using System.Collections.Generic;

namespace game.scripts.db {
    public class KeyIndex<T> where T : GuidIdVo {
        public String name { get; private set; }
        public Type type { get; private set; }
        private Dictionary<Guid, T> index = new Dictionary<Guid, T>();

        public KeyIndex(String name, Type type) {
            this.type = type;
            this.name = name;
        }

        public T getValue(Guid guid) {
            T value;
            index.TryGetValue(guid, out value);
            return value;
        }
    }
}
