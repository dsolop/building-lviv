﻿using System.Collections;
using System.Collections.Generic;
using game.scripts.grid;
using game.scripts.state;
using UnityEngine;

namespace game.scripts {
    public class GameManager : MonoBehaviour {

        private static GameManager instance;
        private GameState currentState;
        private SquareGridMesh grid;

        private float perlinNoiseScale = 4f;

        /// Awake is called when the script instance is being loaded.
        void Awake() {
            if (instance == null) {
                instance = this;
            }

            currentState = new WaitingForInputState();
            grid = new SquareGridMesh(new Vector2Int(100, 100));

            for (int y = 0; y < grid.size.y; y++) {
                for (int x = 0; x < grid.size.x; x++) {
                    int squareIndex = grid.getSquareIndex(x, y);
                    int vertexIndex = squareIndex * Square.VERTEX_PER_SQUARE;
                    // Debug.Log(sample);
                    grid.increaseVertexHeight(vertexIndex + Square.VertexDirection.SW, perlinNoise(x, y), false, false);
                    grid.increaseVertexHeight(vertexIndex + Square.VertexDirection.SE, perlinNoise(x + 1, y), false, false);
                    grid.increaseVertexHeight(vertexIndex + Square.VertexDirection.NW, perlinNoise(x, y + 1), false, false);
                    grid.increaseVertexHeight(vertexIndex + Square.VertexDirection.NE, perlinNoise(x + 1, y + 1), false, false);
                }
            }

            grid.refreshMeshVertices();
        }

        public int perlinNoise(float x, float y) {
            return (int)Mathf.Floor(Mathf.PerlinNoise(x / grid.size.x * perlinNoiseScale, y / grid.size.y * perlinNoiseScale) * 10);
        }

        public static GameManager getInstance() {
            return instance;
        }

        // Use this for initialization
        void Start() {
        }

        // Update is called once per frame
        void Update() {
            if (instance == null) {
                instance = this;
            }
            currentState.update(this);
        }

        public GameState getCurrentState() {
            return currentState;
        }
        public void setCurrentState(GameState state) {
            currentState = state;
        }

        public SquareGridMesh getGrid() {
            return grid;
        }

        public void destroyGameObject(GameObject objectToDestroy) {
            Destroy(objectToDestroy);
        }

        public GameObject clone(GameObject objectToDestroy) {
            return Instantiate(objectToDestroy);
        }

        public T getComponent<T>() {
            return GetComponent<T>();
        }

        // TODO Move to some util??
        // Finds game object by name (not hierarchical!), supports inactive objects
        public GameObject findObject(string name) {
            foreach (GameObject rootObject in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects()) {
                GameObject foundObject = findObject(rootObject, name);
                if (foundObject != null) {
                    return foundObject;
                }
            }
            return null;
        }

        public GameObject findObject(GameObject parent, string name) {
            Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
            foreach (Transform t in trs) {
                if (t.name == name) {
                    return t.gameObject;
                }
            }
            return null;
        }
    }
}