﻿
namespace game.scripts.state {
    public abstract class GameState {
        // TODO DI ???
        protected GameManager gameManager;
        // Update is called once per frame
        public void update(GameManager gameManager) {
            this.gameManager = gameManager;
            update();
        }

        public abstract void update();
    }
}
