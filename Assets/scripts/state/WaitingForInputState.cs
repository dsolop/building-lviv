using log4net;
using UnityEngine;

namespace game.scripts.state {
    public class WaitingForInputState : GameState {
        
        private static readonly ILog log = LogManager.GetLogger(typeof(WaitingForInputState));

        public WaitingForInputState() {
            log.Debug("WaitingForInputState");
        }

        override public void update() {
            if (Input.GetKeyUp(KeyCode.T)) {
                gameManager.setCurrentState(new TerraformingState());
            }
            if (Input.GetKeyUp(KeyCode.B)) {
                gameManager.setCurrentState(new BuildingState());
            }
        }
    }
}