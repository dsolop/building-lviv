using game.scripts.grid;
using log4net;
using UnityEngine;

namespace game.scripts.state {
    public class TerraformingState : GameState {

        private static readonly ILog log = LogManager.GetLogger(typeof(TerraformingState));

        private GameObject selectedVertexSphere;

        private Material sphereMat = null;

        public TerraformingState() {
            log.Debug("TerraformingState");
            sphereMat = (Material)Resources.Load("materials/redSolidMat", typeof(Material));
        }

        override public void update() {
            if (Input.GetKeyUp(KeyCode.PageUp) || Input.GetKeyUp(KeyCode.PageDown)) {
                if (selectedVertexSphere != null) {
                    Vector3 vertex = selectedVertexSphere.transform.position;
                    if (Input.GetKeyUp(KeyCode.PageUp)) {
                        gameManager.getGrid().increaseVertexHeight(vertex);
                        selectedVertexSphere.transform.position += new Vector3(0, GameConstants.zValueScale, 0);
                    } else {
                        gameManager.getGrid().increaseVertexHeight(vertex, -1);
                        selectedVertexSphere.transform.position -= new Vector3(0, GameConstants.zValueScale, 0);
                    }

                    GroundMeshRenderer groundMeshRenderer = GameObject.Find("Ground").GetComponent<GroundMeshRenderer>();
                    groundMeshRenderer.updateMesh();
                }
            }

            if (Input.GetMouseButtonUp(0)) {
                RaycastHit hit;
                // Vector3 input = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 300)) {
                    if (selectedVertexSphere != null) {
                        gameManager.destroyGameObject(selectedVertexSphere);
                        selectedVertexSphere = null;
                    }

                    log.Debug(hit.point);
                    Vector3 vertex = new Vector3(Mathf.Round(hit.point.x), hit.point.y, Mathf.Round(hit.point.z));
                    if (Mathf.Abs(vertex.x - hit.point.x) <= 0.1 && Mathf.Abs(vertex.z - hit.point.z) <= 0.1) {
                        selectedVertexSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        selectedVertexSphere.GetComponent<Renderer>().material = sphereMat;
                        selectedVertexSphere.transform.position = vertex;
                        selectedVertexSphere.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
                    }
                }
            }

            if (Input.GetKeyUp(KeyCode.T) || Input.GetKeyUp(KeyCode.Escape)) {
                if (selectedVertexSphere != null) {
                    gameManager.destroyGameObject(selectedVertexSphere);
                }
                gameManager.setCurrentState(new WaitingForInputState());
            }
        }
    }
}