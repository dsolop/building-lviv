using game.scripts.grid;
using game.scripts.vo;
using game.scripts.vo.configured;
using log4net;
using UnityEngine;

namespace game.scripts.state {

    public class BuildingState : GameState {

        private static readonly ILog log = LogManager.GetLogger(typeof(BuildingState));
        
        private GameObject buildingGridGameObject = null;
        private Material greenPlaneMat = null;
        private Material redPlaneMat = null;
        private BuildingType buildingType = null;
        private int rotate = 0;

        public BuildingState() {
            log.Debug("BuildingState");
            greenPlaneMat = (Material)Resources.Load("materials/greenPlaneMat", typeof(Material));
            redPlaneMat = (Material)Resources.Load("materials/redPlaneMat", typeof(Material));

            buildingType = new BuildingType();
            buildingType.modelName = "3GreenHouse3";
            buildingType.size = new Vector2Int(3, 2);
        }

        override public void update() {
            if (Input.GetKeyUp(KeyCode.B) || Input.GetKeyUp(KeyCode.Escape)) {
                hideGrid();
                gameManager.setCurrentState(new WaitingForInputState());
                return;
            }

            if (Input.GetKeyUp(KeyCode.R)) {
                // TODO 4 - way rotate
                //rotate = rotate == 90 ? 0 : 90; 
            }

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 300)) {
                int buildingX = (int)Mathf.Round(hit.point.x - (float)buildingType.size.x / 2);
                int buildingY = (int)Mathf.Round(hit.point.z - (float)buildingType.size.y / 2);

                Vector2Int buildingCoords = new Vector2Int(buildingX, buildingY);

                if (Input.GetMouseButtonUp(0) && gameManager.getGrid().isBuildingAllowed(buildingCoords, buildingType.size)) {
                    Building building = new Building("House", buildingType, buildingCoords);
                    gameManager.getGrid().addBuilding(building);

                    GameObject house = gameManager.clone(buildingType.model);
                    house.transform.position = new Vector3(building.coords.x, hit.point.y + 1, building.coords.y);
                    // TODO Rotate -- center point??
                    house.SetActive(true);
                }

                redrawGrid(buildingCoords, buildingType.size);
            }
            Resources.FindObjectsOfTypeAll<GameObject>();
        }
        private void hideGrid() {
            if (buildingGridGameObject != null) {
                buildingGridGameObject.SetActive(false);
                gameManager.destroyGameObject(buildingGridGameObject);
            }
        }

        private void redrawGrid(Vector2Int buildingCoords, Vector2Int buildingSize) {
            hideGrid();

            Material buildingGridMaterial = redPlaneMat;

            // TODO cache isBuildingAllowed call?
            if (gameManager.getGrid().isBuildingAllowed(buildingCoords, buildingSize)) {
                buildingGridMaterial = greenPlaneMat;
            }
            SquareGridMesh subgrid = gameManager.getGrid().createSubGrid(buildingCoords, buildingSize);
            log.Debug(subgrid);
            // SquareGridMesh gridMesh = new SquareGridMesh(subgrid);
            buildingGridGameObject = new GameObject("BuildingGrid");
            buildingGridGameObject.transform.position = new Vector3(buildingCoords.x, 0.001f, buildingCoords.y);

            MeshFilter meshFilter = buildingGridGameObject.AddComponent<MeshFilter>();
            meshFilter.mesh = subgrid.getMesh();

            MeshRenderer meshRenderer = buildingGridGameObject.AddComponent<MeshRenderer>();
            meshRenderer.material = buildingGridMaterial;
        }
    }
}