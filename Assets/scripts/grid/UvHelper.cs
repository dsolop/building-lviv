using UnityEngine;
using System;
using log4net;

namespace game.scripts.grid {
    public class UvHelper {

        private static readonly ILog log = LogManager.GetLogger(typeof(UvHelper));

        int unitsPerSide;
        float oneUnitScale; 

        public UvHelper (int unitsPerSide) {
            this.unitsPerSide = unitsPerSide;
            oneUnitScale = 1f / unitsPerSide;
        }
        
        public Vector2[] getUvForSquare(Vector2Int coords, TerrainType terrainType) {
            Vector2Int textureCoords = terrainType.textureCoords;
            int textureBlockSize = terrainType.textureBlockSize;
            float offsetX = (coords.x % textureBlockSize) * oneUnitScale;
            float offsetY = (coords.y % textureBlockSize) * oneUnitScale;

            Vector2[] uv = new Vector2[Square.VERTEX_PER_SQUARE];
            uv[Square.VertexDirection.SE] = new Vector2(textureCoords.x * oneUnitScale + offsetX, 
                                                        textureCoords.y * oneUnitScale + offsetY);

            uv[Square.VertexDirection.SW] = new Vector2((textureCoords.x + 1) * oneUnitScale + offsetX, 
                                                        textureCoords.y * oneUnitScale + offsetY);

            uv[Square.VertexDirection.NE] = new Vector2(textureCoords.x * oneUnitScale + offsetX, 
                                                        (textureCoords.y + 1) * oneUnitScale + offsetY);

            uv[Square.VertexDirection.NW] = new Vector2((textureCoords.x + 1) * oneUnitScale + offsetX, 
                                                        (textureCoords.y + 1) * oneUnitScale + offsetY);

            uv[Square.VertexDirection.CENTER] = new Vector2((textureCoords.x + 0.5f) * oneUnitScale + offsetX, 
                                                            (textureCoords.y + 0.5f) * oneUnitScale + offsetY);
            log.Trace(
                "UVS. SE: " + uv[Square.VertexDirection.SE] +
                "UVS. SW: " + uv[Square.VertexDirection.SW] +
                "UVS. NE: " + uv[Square.VertexDirection.NE] +
                "UVS. NW: " + uv[Square.VertexDirection.NW] +
                "UVS. CENTER: " + uv[Square.VertexDirection.CENTER]
            );
            return uv;
        }
    }
}