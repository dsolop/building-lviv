﻿using System;
using System.Linq;
using game.scripts.vo;
using UnityEngine;

namespace game.scripts.grid {
    public class Square {
        public const int VERTEX_PER_SQUARE = 5;

        public class VertexDirection {
            public const int SW = 0;
            public const int SE = 1;
            public const int NW = 2;
            public const int NE = 3;
            public const int CENTER = 4;
        }

        // contains 4 vertex heighs for each corner. Doesn't contains center height
        public int[] vertexHeights { get; private set; }

        public Building building { get; set; }

        public TerrainType terrainType {get; set; }

        public Square(int swHeigt, int seHeigt, int nwHeigt, int neHeigt) {
            vertexHeights = new int[] {swHeigt, seHeigt, nwHeigt, neHeigt};
        }

        public Square() {
            vertexHeights = new int[] {0, 0, 0, 0};
        }

        public bool isFlat() {
            return (vertexHeights[VertexDirection.SE] == vertexHeights[VertexDirection.SW]) &&
             (vertexHeights[VertexDirection.SE] == vertexHeights[VertexDirection.NE]) &&
             (vertexHeights[VertexDirection.SE] == vertexHeights[VertexDirection.NW]);
        }
    }
}

