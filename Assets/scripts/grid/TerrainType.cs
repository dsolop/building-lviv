using System.Collections.Generic;
using UnityEngine;

namespace game.scripts.grid {
    public class TerrainType {
        public string name { get; private set; }
        public Vector2Int textureCoords { get; private set; }
        public int textureBlockSize { get; private set; }

        public static Dictionary<string, TerrainType> terrainTypes { get; private set; }

        static TerrainType() {
            terrainTypes = new Dictionary<string, TerrainType>();
            terrainTypes.Add("grass", new TerrainType("grass", new Vector2Int(0, 0), 4));
            terrainTypes.Add("stone", new TerrainType("stone", new Vector2Int(0, 0), 4));
        }
        
        public TerrainType(string name, Vector2Int textureCoords, int textureBlockSize) {
            this.name = name;
            this.textureCoords = textureCoords;
            this.textureBlockSize = textureBlockSize;
        }
    }
}