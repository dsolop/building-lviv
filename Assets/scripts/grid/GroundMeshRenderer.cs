﻿using log4net;
using UnityEngine;

namespace game.scripts.grid {
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
    public class GroundMeshRenderer : MonoBehaviour {

        private static readonly ILog log = LogManager.GetLogger(typeof(GroundMeshRenderer));
        
        private SquareGridMesh gridMesh;

        // Use this for initialization
        void Start() {
            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            var grid = gameManager.getGrid();
            log.Debug(grid);
            gridMesh = new SquareGridMesh(grid);
            updateMesh();
        }

        public void updateMesh() {
            GetComponent<MeshFilter>().mesh = gridMesh.getMesh();
            GetComponent<MeshCollider>().sharedMesh = gridMesh.getMesh();

            Material planeMat = (Material)Resources.Load("materials/planeMat", typeof(Material));
            GetComponent<MeshRenderer>().material = planeMat;
        }
    }
}