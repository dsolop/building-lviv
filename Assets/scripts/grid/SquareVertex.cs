using System;
using UnityEngine;

namespace game.scripts.grid
{
    public class SquareVertex
    {
        public int vertexIndex { get; private set; }
        public int squareIndex { get; private set; }
        public int vertexDirection { get; private set; }

        public Vector2Int gridSize { get; private set; }
        public Vector2Int squareCoords { get; private set; }

        public SquareVertex(int vertexIndex, Vector2Int gridSize) {
            if (gridSize == null || gridSize.x < 0 || gridSize.y < 0) {
                throw new ArgumentException("Incorrect or null grid size: " + gridSize);
            }

            if (vertexIndex < 0 || vertexIndex >= gridSize.x * gridSize.y * Square.VERTEX_PER_SQUARE) {
                throw new ArgumentException("Incorrect vertex index " + vertexIndex + " for grid size " + gridSize);
            }

            this.squareIndex = vertexIndex / Square.VERTEX_PER_SQUARE;
            this.vertexDirection = vertexIndex - squareIndex * Square.VERTEX_PER_SQUARE;
            this.vertexIndex = vertexIndex;
            this.gridSize = gridSize;

            int y = squareIndex / gridSize.x;
            this.squareCoords = new Vector2Int(squareIndex - y * gridSize.x, y);;
        }

        public SquareVertex[] getSameCoordsNeighbours() {
            int totalVertices = gridSize.x * gridSize.y * Square.VERTEX_PER_SQUARE;
            if (vertexDirection == Square.VertexDirection.SW) {
                int neighbourVertexIndex1 = vertexIndex - Square.VERTEX_PER_SQUARE + 1;
                int neighbourVertexIndex2 = vertexIndex - gridSize.x * Square.VERTEX_PER_SQUARE + 2;
                int neighbourVertexIndex3 = vertexIndex - gridSize.x * Square.VERTEX_PER_SQUARE - 2;
                return new [] { 
                    neighbourVertexIndex1 >= 0 && squareCoords.x > 0 ? new SquareVertex(neighbourVertexIndex1, gridSize) : null, 
                    neighbourVertexIndex2 >= 0 ? new SquareVertex(neighbourVertexIndex2, gridSize) : null, 
                    neighbourVertexIndex3 >= 0 && squareCoords.x > 0 ? new SquareVertex(neighbourVertexIndex3, gridSize) : null
                };
            } else if (vertexDirection == Square.VertexDirection.SE) {
                int neighbourVertexIndex1 = vertexIndex + Square.VERTEX_PER_SQUARE - 1;
                int neighbourVertexIndex2 = vertexIndex - gridSize.x * Square.VERTEX_PER_SQUARE + 2;
                int neighbourVertexIndex3 = vertexIndex - (gridSize.x - 1) * Square.VERTEX_PER_SQUARE + 1;
                return new [] { 
                    neighbourVertexIndex1 < totalVertices && squareCoords.x <  gridSize.x - 1 ? new SquareVertex(neighbourVertexIndex1, gridSize) : null, 
                    neighbourVertexIndex2 >= 0 ? new SquareVertex(neighbourVertexIndex2, gridSize) : null, 
                    neighbourVertexIndex3 >= 0 && squareCoords.x <  gridSize.x - 1 ? new SquareVertex(neighbourVertexIndex3, gridSize) : null
                };
            } else if (vertexDirection == Square.VertexDirection.NW) {
                int neighbourVertexIndex1 = vertexIndex - Square.VERTEX_PER_SQUARE + 1;
                int neighbourVertexIndex2 = vertexIndex + gridSize.x * Square.VERTEX_PER_SQUARE - 2;
                int neighbourVertexIndex3 = vertexIndex + (gridSize.x - 1) * Square.VERTEX_PER_SQUARE - 1;
                return new [] { 
                    neighbourVertexIndex1 >= 0 && squareCoords.x > 0 ? new SquareVertex(neighbourVertexIndex1, gridSize) : null, 
                    neighbourVertexIndex2 < totalVertices ? new SquareVertex(neighbourVertexIndex2, gridSize) : null, 
                    neighbourVertexIndex3 < totalVertices  && squareCoords.x > 0 ? new SquareVertex(neighbourVertexIndex3, gridSize) : null
                };
            } else if (vertexDirection == Square.VertexDirection.NE) {
                int neighbourVertexIndex1 = vertexIndex + Square.VERTEX_PER_SQUARE - 1;
                int neighbourVertexIndex2 = vertexIndex + gridSize.x * Square.VERTEX_PER_SQUARE - 2;
                int neighbourVertexIndex3 = vertexIndex + gridSize.x * Square.VERTEX_PER_SQUARE + 2;
                return new [] { 
                    neighbourVertexIndex1 < totalVertices && squareCoords.x < gridSize.x - 1 ? new SquareVertex(neighbourVertexIndex1, gridSize) : null, 
                    neighbourVertexIndex2 < totalVertices ? new SquareVertex(neighbourVertexIndex2, gridSize) : null, 
                    neighbourVertexIndex3 < totalVertices && squareCoords.x < gridSize.x - 1 ? new SquareVertex(neighbourVertexIndex3, gridSize) : null
                };
            } else {
                return new SquareVertex[0];
            }
        }

        public SquareVertex[] getNeighbours() {
            int totalVertices = gridSize.x * gridSize.y * Square.VERTEX_PER_SQUARE;
            if (vertexDirection == Square.VertexDirection.SW) {
                // corners
                int neighbourVertexIndex1 = vertexIndex + 1;
                int neighbourVertexIndex2 = vertexIndex + 2;
                int neighbourVertexIndex3 = vertexIndex - Square.VERTEX_PER_SQUARE;
                int neighbourVertexIndex4 = vertexIndex - gridSize.x * Square.VERTEX_PER_SQUARE;
                // center
                int neighbourVertexIndex5 = vertexIndex + 4;
                int neighbourVertexIndex6 = vertexIndex - 1;
                int neighbourVertexIndex7 = vertexIndex - gridSize.x * Square.VERTEX_PER_SQUARE + 4;
                int neighbourVertexIndex8 = vertexIndex - gridSize.x * Square.VERTEX_PER_SQUARE - 1;
                return new [] { 
                    new SquareVertex(neighbourVertexIndex1, gridSize), 
                    new SquareVertex(neighbourVertexIndex2, gridSize), 
                    neighbourVertexIndex3 >= 0 && squareCoords.x > 0 ? new SquareVertex(neighbourVertexIndex3, gridSize) : null, 
                    neighbourVertexIndex4 >= 0 ? new SquareVertex(neighbourVertexIndex4, gridSize) : null,
                    new SquareVertex(neighbourVertexIndex5, gridSize),
                    neighbourVertexIndex6 >= 0 && squareCoords.x > 0  ? new SquareVertex(neighbourVertexIndex6, gridSize) : null,
                    neighbourVertexIndex7 >= 0 ? new SquareVertex(neighbourVertexIndex7, gridSize) : null,
                    neighbourVertexIndex8 >= 0 && squareCoords.x > 0 ? new SquareVertex(neighbourVertexIndex8, gridSize) : null
                };
            } else if (vertexDirection == Square.VertexDirection.SE) {
                // corners
                int neighbourVertexIndex1 = vertexIndex - 1;
                int neighbourVertexIndex2 = vertexIndex + 2;
                int neighbourVertexIndex3 = vertexIndex + Square.VERTEX_PER_SQUARE;
                int neighbourVertexIndex4 = vertexIndex - gridSize.x * Square.VERTEX_PER_SQUARE;
                // center
                int neighbourVertexIndex5 = vertexIndex + 3;
                int neighbourVertexIndex6 = vertexIndex + Square.VERTEX_PER_SQUARE + 3;
                int neighbourVertexIndex7 = vertexIndex - gridSize.x * Square.VERTEX_PER_SQUARE + 3;
                int neighbourVertexIndex8 = vertexIndex - (gridSize.x - 1) * Square.VERTEX_PER_SQUARE + 3;
                return new [] { 
                    new SquareVertex(neighbourVertexIndex1, gridSize), 
                    new SquareVertex(neighbourVertexIndex2, gridSize), 
                    neighbourVertexIndex3 < totalVertices && squareCoords.x <  gridSize.x - 1 ? new SquareVertex(neighbourVertexIndex3, gridSize) : null, 
                    neighbourVertexIndex4 >= 0 ? new SquareVertex(neighbourVertexIndex4, gridSize) : null,
                    new SquareVertex(neighbourVertexIndex5, gridSize),
                    neighbourVertexIndex6 < totalVertices && squareCoords.x <  gridSize.x - 1 ? new SquareVertex(neighbourVertexIndex6, gridSize) : null, 
                    neighbourVertexIndex7 > 0 ? new SquareVertex(neighbourVertexIndex7, gridSize) : null, 
                    neighbourVertexIndex8 > 0 && squareCoords.x <  gridSize.x - 1 ? new SquareVertex(neighbourVertexIndex8, gridSize) : null 
                };
            } else if (vertexDirection == Square.VertexDirection.NW) {
                // corners
                int neighbourVertexIndex1 = vertexIndex - 2;
                int neighbourVertexIndex2 = vertexIndex + 1;
                int neighbourVertexIndex3 = vertexIndex - Square.VERTEX_PER_SQUARE;
                int neighbourVertexIndex4 = vertexIndex + gridSize.x * Square.VERTEX_PER_SQUARE;
                // center
                int neighbourVertexIndex5 = vertexIndex + 2;
                int neighbourVertexIndex6 = vertexIndex - Square.VERTEX_PER_SQUARE + 2;
                int neighbourVertexIndex7 = vertexIndex + gridSize.x * Square.VERTEX_PER_SQUARE + 2;
                int neighbourVertexIndex8 = vertexIndex + (gridSize.x - 1) * Square.VERTEX_PER_SQUARE + 2;
                return new [] { 
                    new SquareVertex(neighbourVertexIndex1, gridSize), 
                    new SquareVertex(neighbourVertexIndex2, gridSize), 
                    neighbourVertexIndex3 >= 0 && squareCoords.x > 0 ? new SquareVertex(neighbourVertexIndex3, gridSize) : null, 
                    neighbourVertexIndex4 < totalVertices ? new SquareVertex(neighbourVertexIndex4, gridSize) : null,
                    new SquareVertex(neighbourVertexIndex5, gridSize), 
                    neighbourVertexIndex6 >= 0 && squareCoords.x > 0 ? new SquareVertex(neighbourVertexIndex6, gridSize) : null, 
                    neighbourVertexIndex7 < totalVertices ? new SquareVertex(neighbourVertexIndex7, gridSize) : null,
                    neighbourVertexIndex8 < totalVertices && squareCoords.x > 0 ? new SquareVertex(neighbourVertexIndex8, gridSize) : null,
                };
            } else if (vertexDirection == Square.VertexDirection.NE) {
                // corners
                int neighbourVertexIndex1 = vertexIndex - 1;
                int neighbourVertexIndex2 = vertexIndex - 2;
                int neighbourVertexIndex3 = vertexIndex + Square.VERTEX_PER_SQUARE;
                int neighbourVertexIndex4 = vertexIndex + gridSize.x * Square.VERTEX_PER_SQUARE;
                // center
                int neighbourVertexIndex5 = vertexIndex + 1;
                int neighbourVertexIndex6 = vertexIndex + Square.VERTEX_PER_SQUARE + 1;
                int neighbourVertexIndex7 = vertexIndex + gridSize.x * Square.VERTEX_PER_SQUARE + 1;
                int neighbourVertexIndex8 = vertexIndex + (gridSize.x + 1) * Square.VERTEX_PER_SQUARE + 1;
                return new [] { 
                    new SquareVertex(neighbourVertexIndex1, gridSize), 
                    new SquareVertex(neighbourVertexIndex2, gridSize), 
                    neighbourVertexIndex3 < totalVertices  && squareCoords.x <  gridSize.x - 1 ? new SquareVertex(neighbourVertexIndex3, gridSize) : null, 
                    neighbourVertexIndex4 < totalVertices ? new SquareVertex(neighbourVertexIndex4, gridSize) : null,
                    new SquareVertex(neighbourVertexIndex5, gridSize),
                    neighbourVertexIndex6 < totalVertices && squareCoords.x <  gridSize.x - 1 ? new SquareVertex(neighbourVertexIndex6, gridSize) : null, 
                    neighbourVertexIndex7 < totalVertices ? new SquareVertex(neighbourVertexIndex7, gridSize) : null, 
                    neighbourVertexIndex8 < totalVertices && squareCoords.x <  gridSize.x - 1 ? new SquareVertex(neighbourVertexIndex8, gridSize) : null 
                };
            } else if (vertexDirection == Square.VertexDirection.CENTER) {
                return new [] { 
                    new SquareVertex(vertexIndex - 1, gridSize), 
                    new SquareVertex(vertexIndex - 2, gridSize), 
                    new SquareVertex(vertexIndex - 3, gridSize), 
                    new SquareVertex(vertexIndex - 4, gridSize)
                };
            } else {
                // will never happen
                return new SquareVertex[0];
            }
        }

        public override bool Equals(object obj) {
            if (obj == null || this.GetType() != obj.GetType()) {
                return false;
            }
            SquareVertex that = (SquareVertex) obj;
            return gridSize.Equals(that.gridSize) && vertexIndex == that.vertexIndex;
        }
        
        public override int GetHashCode() {
            return vertexIndex.GetHashCode();
        }

        public override string ToString () {
            return "SquareVertex[" + vertexIndex + "]";
        }
    }
}