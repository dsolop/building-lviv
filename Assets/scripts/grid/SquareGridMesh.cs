﻿using System.Collections.Generic;
using System.Linq;
using game.scripts.vo;
using log4net;
using UnityEngine;

namespace game.scripts.grid
{
    public class SquareGridMesh {

        private static readonly ILog log = LogManager.GetLogger(typeof(SquareGridMesh));

        public Vector2Int size { get; private set; }
        public Square[] squares { get; private set; }
        private Vector3[] vertices;
        private Vector2[] uv;
        private Mesh mesh;
        private UvHelper gridUvHelper = new UvHelper(4);

        public SquareGridMesh(SquareGridMesh squareGridMesh) {
            size = squareGridMesh.size;
            squares = squareGridMesh.squares;
            vertices = squareGridMesh.vertices;
            uv = squareGridMesh.uv;
            mesh = squareGridMesh.mesh;
        }
        public SquareGridMesh(Vector2Int size, Square[] squares) {
            this.squares = squares;
            this.size = size;
            generateMash();
        }
        
        public SquareGridMesh(Vector2Int size) {
            this.size = size;

            squares = new Square[size.x * size.y];
            for (int y = 0; y < size.y; y++) {
                for (int x = 0; x < size.x; x++) {
                    int squareIndex = getSquareIndex(x, y);
                    squares[squareIndex] = new Square();
                    squareIndex++;
                }
            }

            generateMash();
        }

        public Mesh getMesh() {
            return mesh;
        }

        public Square getSquareSafe(int x, int y) {
            int squareIndex = getSquareIndex(Mathf.Clamp(x, 0, size.x - 1), Mathf.Clamp(y, 0, size.y - 1) );
            return squares[squareIndex];
        }

        public int getSquareIndex(int x, int y) {
            return getSquareIndex(x, y, this.size);
        }

        public int getSquareIndex(int x, int y, Vector2Int size) {
            return y * size.x + x;
        }

        public Vector2Int getSquareCoords(int squareIndex) {
            int y = squareIndex / size.x;
            return new Vector2Int(squareIndex - y * size.x, y);;
        }
        public Square getSquare(int x, int y) {
            return squares[getSquareIndex(x, y)];
        }
        public Square[] getRects() {
            return squares;
        }

        public int getVertexIndex(Vector3 vertex) {
            // TODO do not do a loop, calc a vertex
            for (int i = 0; i < size.x * size.y * Square.VERTEX_PER_SQUARE; i++) {
                if (Mathf.Abs(vertices[i].x - vertex.x) < 0.1 && Mathf.Abs(vertices[i].z - vertex.z) < 0.1) {
                    vertices[i] = vertex;
                    return i;
                }
            }

            log.Debug("verrtex index: -1");
            log.Debug(vertex);
            return -1;
        }

        public void increaseVertexHeight(Vector3 vertex, int times = 1) {
            increaseVertexHeight(getVertexIndex(vertex), times, true);

        }

        public void increaseVertexHeight(int vertexIndex, int times, bool updateNeightbourVertices, bool refreshMeshVertices = true) {
            SquareVertex currentSquareVertex = new SquareVertex(vertexIndex, size);
            int[] vertexHeights = squares[currentSquareVertex.squareIndex].vertexHeights;

            vertexHeights[currentSquareVertex.vertexDirection] += times;
            updateSquareVertices(getSquareCoords(currentSquareVertex.squareIndex));

            if (updateNeightbourVertices) {
                var neighbours = currentSquareVertex.getSameCoordsNeighbours();
                foreach (var neighbour in neighbours)  {
                    if (neighbour == null) {
                        continue;
                    }
                    squares[neighbour.squareIndex].vertexHeights[neighbour.vertexDirection] = vertexHeights[currentSquareVertex.vertexDirection];
                    updateSquareVertices(getSquareCoords(neighbour.squareIndex));
                }
            }
        
            if (refreshMeshVertices) {
                this.refreshMeshVertices();
            }
        }

        
        public void refreshMeshVertices() {
            mesh.vertices = vertices;
            mesh.uv = uv;
            mesh.normals = recalculateNormals();
        }

        private Vector3[] recalculateNormals() {
            Vector3[] normals = new Vector3[vertices.Length];

            for (int vertexIndex = 0; vertexIndex < vertices.Length; vertexIndex++) {
                SquareVertex squareVertex = new SquareVertex(vertexIndex, size);
                // see https://computergraphics.stackexchange.com/a/4032
                Vector3 a = vertices[vertexIndex];
                SquareVertex[] neighbours = squareVertex.getNeighbours();
                Vector3 normal = new Vector3(0.0f, 0.0f, 0.0f);
                // todo double check loop
                for (int i = 0; i < neighbours.Length - 1; i++) {
                    var bSquareVertex = neighbours[i];
                    if (bSquareVertex == null) {
                        continue;
                    }
                    var b = vertices[bSquareVertex.vertexIndex];
                    for (int j = i + 1; j < neighbours.Length; j++) {
                        var cSquareVertex = neighbours[j];
                        if (cSquareVertex == null) {
                            continue;
                        }
                        var c = vertices[cSquareVertex.vertexIndex];

                        normal += abs(Vector3.Cross(b - a, c - a));
                    }
                }

                // var b = vertices[neighbours[0].vertexIndex];
                // var c = vertices[neighbours[1].vertexIndex];
                // normal = abs(Vector3.Cross(b - a, c - a));
                Debug.Assert(normal.x != 0 || normal.y != 0 || normal.z != 0);
                normal.Normalize();
                // log.Debug(
                //     "Normal: " + normal.ToString() +
                //     " Neighbours: " + string.Join(",", neighbours.Select(p => p!= null?p.ToString():null).ToArray())
                // );
                normals[vertexIndex] = normal;
            }
                    
            return normals;
        }
        // todo extract
        public Vector3 abs (Vector3 vector3){
            return new Vector3(Mathf.Abs(vector3.x), Mathf.Abs(vector3.y), Mathf.Abs(vector3.z));
        }

        private void generateMash() {
            mesh = new Mesh();
            vertices = new Vector3[Square.VERTEX_PER_SQUARE * size.x * size.y];
            uv = new Vector2[vertices.Length];

            vertices = new Vector3[Square.VERTEX_PER_SQUARE * size.x * size.y];
            uv = new Vector2[vertices.Length];
            int[] triangles = new int[size.x * size.y * 4 * 3];

            int squareIndex = 0;
            int triangleIndex = 0;
            for (int y = 0; y < size.y; y++) {
                for (int x = 0; x < size.x; x++) {
                    updateSquareVertices(x, y);
                    updateSquareVerticesUV(x, y);
                    
                    int vertexIndex = squareIndex * Square.VERTEX_PER_SQUARE;

                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.SW;
                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.NW;
                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.CENTER;

                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.SW;
                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.CENTER;
                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.SE;

                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.SE;
                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.CENTER;
                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.NE;

                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.NE;
                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.CENTER;
                    triangles[triangleIndex++] = vertexIndex + Square.VertexDirection.NW;

                    squareIndex++;
                }
            }

            mesh.vertices = vertices;
            mesh.uv = uv;
            mesh.triangles = triangles;

            mesh.normals = recalculateNormals();
            mesh.name = "Procedural Grid";
        
        }

        public void updateSquareVerticesUV(int squareX, int squareY) {
            int squareIndex = getSquareIndex(squareX, squareY);
            int vertexIndex = squareIndex * Square.VERTEX_PER_SQUARE;
            // todo extract to some UV helpers and sprite entities

            TerrainType terrainType = TerrainType.terrainTypes["grass"];
            Vector3 vertex = vertices[vertexIndex];
            if (vertex.y > 3) {
                terrainType = TerrainType.terrainTypes["stone"];
            }

            Vector2[] squareUvs = gridUvHelper.getUvForSquare(new Vector2Int(squareX, squareY), terrainType);
            int squareUvIndex = vertexIndex;
            foreach (Vector2 squareUv in squareUvs) {
                uv[squareUvIndex++] = squareUv;
            }
        }

        public void updateSquareVertices(Vector2Int squareCoords) {
            updateSquareVertices(squareCoords.x, squareCoords.y);
        }

        public void updateSquareVertices(int squareX, int squareY) {
            int squareIndex = getSquareIndex(squareX, squareY);
            int vertexIndex = squareIndex * Square.VERTEX_PER_SQUARE;
            int[] vertexHeights = squares[squareIndex].vertexHeights;

            vertices[vertexIndex + Square.VertexDirection.SW] =
                    new Vector3(squareX, GameConstants.zValueScale * vertexHeights[Square.VertexDirection.SW], squareY);
                    
            vertices[vertexIndex + Square.VertexDirection.SE] =
                    new Vector3(squareX + 1, GameConstants.zValueScale * vertexHeights[Square.VertexDirection.SE], squareY);
            
            vertices[vertexIndex + Square.VertexDirection.NW] =
                    new Vector3(squareX, GameConstants.zValueScale * vertexHeights[Square.VertexDirection.NW], squareY + 1);
            
            vertices[vertexIndex + Square.VertexDirection.NE] =
                    new Vector3(squareX + 1, GameConstants.zValueScale * vertexHeights[Square.VertexDirection.NE], squareY + 1);
            
            updateCenterVertex(squareX, squareY);
        }

        private void updateCenterVertex(int squareX, int squareY) {
            int squareIndex = getSquareIndex(squareX, squareY);
            Square square = squares[squareIndex];
            int[] heigths = new [] {
                square.vertexHeights[Square.VertexDirection.SE],
                square.vertexHeights[Square.VertexDirection.SW],
                square.vertexHeights[Square.VertexDirection.NE],
                square.vertexHeights[Square.VertexDirection.NW]
            };

            var heigthsWithCount = new List<int>(heigths)
                        .GroupBy(heigth => heigth)
                        .Select(group => new {
                            count = group.Count(), 
                            heigth = group.Key
                        })
                        .OrderByDescending(heigthWithCount => heigthWithCount.count)
                        .ToList();

            int maxCount = heigthsWithCount[0].count;
            int maxCountHeight = heigthsWithCount[0].heigth;

            int vertexIndex = squareIndex * Square.VERTEX_PER_SQUARE;
            float vertexHeight;
            if (maxCount >= 3) {
                // Debug.Log("3 same objects");
                vertexHeight = maxCountHeight * GameConstants.zValueScale;

                vertices[vertexIndex + Square.VertexDirection.CENTER] =
                        new Vector3(squareX + 0.5f, vertexHeight, squareY + 0.5f);
            } else {
                Vector3 position = vertices[vertexIndex + Square.VertexDirection.SW];
                Vector3 ne = vertices[vertexIndex + Square.VertexDirection.NE];
                Vector3 direction = new Vector3(ne.x - position.x, ne.y - position.y, ne.z - position.z);

                // https://math.stackexchange.com/questions/404440/what-is-the-equation-for-a-3d-line/404446#404446
                float x = squareX + 0.5f;
                float y = (x - position.x) / direction.x * direction.y + position.y;
                float z = (x - position.x) / direction.x * direction.z + position.z;

                vertices[vertexIndex + Square.VertexDirection.CENTER] = new Vector3(x, y, z);
            }
            
        }
        internal void addBuilding(Building building) {
            Vector2Int buildingSize = building.buildingType.size;
            for (int y = building.coords.y; y < building.coords.y + buildingSize.y; y++) {
                for (int x = building.coords.x; x < building.coords.x + buildingSize.x; x++) {
                    var square = squares[getSquareIndex(x, y)];
                    square.building = building;
                }
            }
        }

        public bool isBuildingAllowed(Vector2Int buildingCoords, Vector2Int buildingSize) {
            // TODO range check
            int firstSquareIndex = getSquareIndex(buildingCoords.x, buildingCoords.y);
            for (int y = buildingCoords.y; y < buildingCoords.y + buildingSize.y; y++) {
                for (int x = buildingCoords.x; x < buildingCoords.x + buildingSize.x; x++) {
                    Square square = squares[getSquareIndex(x, y)];

                    bool isFlat = square.isFlat();
                    if (!isFlat) {
                        return false;
                    }

                    if (square.building != null) {
                        return false;
                    }
                }
            }

            return true;
        }

        public SquareGridMesh createSubGrid(Vector2Int coords, Vector2Int size) {
            Square[] newSquares = new Square[size.x * size.y];
            log.Debug(coords);
            for (int y = coords.y; y < coords.y + size.y; y++) {
                for (int x = coords.x; x < coords.x + size.x; x++) {
                    newSquares[getSquareIndex(x - coords.x, y - coords.y, size)] = squares[getSquareIndex(x, y)];
                    log.Debug(
                        squares[getSquareIndex(x, y)].vertexHeights[0] + " " + 
                        squares[getSquareIndex(x, y)].vertexHeights[1] + " " + 
                        squares[getSquareIndex(x, y)].vertexHeights[2] + " " + 
                        squares[getSquareIndex(x, y)].vertexHeights[3] + " " 
                    );
                }
            }

            log.Debug(newSquares);
            return new SquareGridMesh(size, newSquares);
        }
    }
}