using System;

namespace game.scripts.vo {
    
    [Serializable]
    public class FinancialAsset : Sellable {
        public Business business { get; private set; }
        public int sharePercent { get; private set; }

        public FinancialAsset(string name, Business business, int sharePercent) : base(name) {
            this.business = business;
            this.sharePercent = sharePercent;
        }
    }
}