﻿using System;
namespace game.scripts.vo {
    public class PersonNeeds {
        public byte foodAvailable { get; set; }
        public byte foodVariety { get; set; }
        public byte clothes { get; set; }
        public byte homeQuality { get; set; }
        public byte jobQuality { get; set; }
        public byte healthcare { get; set; }
        public byte religion { get; set; }
        public byte entertainment { get; set; }
        public byte education { get; set; }
    }
}
