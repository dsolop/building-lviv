﻿using System;
namespace game.scripts.vo {

    [Serializable]
    public class Good : Sellable {

        public Building storedAt { get; private set; }
        public GoodStatus status { get; private set; }

        public Good(string name, Building storedAt) : base(name) {
            this.storedAt = storedAt;
            status = GoodStatus.STORING;
        }
    }

    public enum GoodStatus {
        STORING, TRANSFERRING
    }
}
