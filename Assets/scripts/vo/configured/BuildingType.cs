using System;
using UnityEngine;

namespace game.scripts.vo.configured {

    [Serializable]
    public class BuildingType : NamedVo {
        public GameObject model { 
            get { 
                return GameManager.getInstance().findObject(modelName);
            } 
            private set {} 
        }
        public String modelName { get; set; }
        public Vector2Int size { get; set; }
    }
}