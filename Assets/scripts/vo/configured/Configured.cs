using System;
using System.Collections.Generic;

namespace game.scripts.vo.configured {

    [Serializable]
    public class Configured {
        public Dictionary<string, BuildingType> buildingTypes { get; set; }
        public Dictionary<string, GoodType> goodTypes { get; set; }
        public Dictionary<string, ResourceRecipe> resourceRecipes { get; set; }

        public Configured() {
            buildingTypes = new Dictionary<string, BuildingType>();
            goodTypes = new Dictionary<string, GoodType>();
            resourceRecipes = new Dictionary<string, ResourceRecipe>();
        }

        public BuildingType getBuildingType(string name) {
            BuildingType buildingType = null;
            buildingTypes.TryGetValue(name, out buildingType);
            return buildingType;
        }

        public void addBuildingType(BuildingType buildingType) {
            buildingTypes.Add(buildingType.name, buildingType);
        }

        public GoodType getGoodType(string name) {
            GoodType goodType = null;
            goodTypes.TryGetValue(name, out goodType);
            return goodType;
        }

        public void addGoodType(GoodType goodType) {
            goodTypes.Add(goodType.name, goodType);
        }

        public ResourceRecipe getResourceRecipe(string name) {
            ResourceRecipe resourceRecipe = null;
            resourceRecipes.TryGetValue(name, out resourceRecipe);
            return resourceRecipe;
        }

        public void addResourceRecipe(ResourceRecipe resourceRecipe) {
            resourceRecipes.Add(resourceRecipe.name, resourceRecipe);
        }
        
    }
}