namespace game.scripts.vo.configured {

    // name is PK
    public class NamedVo {
        public string name { get; set; }
        public string label { get; set; }

        public NamedVo() {
        }

        public NamedVo(string name) : this() {
            this.name = name;
        }

        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType()) {
                return false;
            }
            return ((GoodType)obj).name.Equals(this.name);
        }
        
        public override int GetHashCode() {
            return name.GetHashCode();
        }
    }
}