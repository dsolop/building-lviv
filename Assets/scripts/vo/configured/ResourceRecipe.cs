using System;
using System.Collections.Generic;

namespace game.scripts.vo.configured {

    [Serializable]
    public class ResourceRecipe : NamedVo {
        public Dictionary<GoodType, int> inputResources { get; set; }
        public Dictionary<GoodType, int> outputResources { get; set; }
        public int productionTimeHours { get; set; }
    }
}