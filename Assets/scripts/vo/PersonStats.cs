﻿using System;
namespace game.scripts.vo {
    public class PersonStats {
        public byte srength { get; set; }
        public byte dexterity { get; set; }
        public byte constitution { get; set; }
        public byte charisma { get; set; }
        public byte intelligence { get; set; }
        public byte wisdom { get; set; }
        public byte speed { get; set; }
        public byte luck { get; set; }
    }
}
