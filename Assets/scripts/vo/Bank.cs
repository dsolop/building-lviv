using System;

namespace game.scripts.vo {

    [Serializable]
    public class Bank : Business {
        public Bank(string name, BusinessUnit owner) : base(name, owner) {
        }
    }
}