using System;
using System.Collections;
using System.Collections.Generic;

namespace game.scripts.vo {
    [Serializable]
    public class Business : BusinessUnit {

        public HashSet<Building> buildings { get; private set; }

        public Business(string name, BusinessUnit owner) : base(name) {
            buildings = new HashSet<Building>();
        }

        public bool addBuilding(Building building) {
            return buildings.Add(building);
        }
    }
}