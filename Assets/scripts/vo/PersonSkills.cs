﻿using System;
namespace game.scripts.vo {
    public class PersonSkills {
        public byte building { get; set; }
        public byte transporting { get; set; }
        public byte bookkeeping { get; set; }
        public byte business { get; set; }
        public byte selling { get; set; }
        public byte woodworking { get; set; }
        public byte farming { get; set; }
        public byte milling { get; set; }
        public byte baking { get; set; }
    }
}
