using System;

namespace game.scripts.vo {
    [Serializable]
    public class Resource : Good {
        public Resource(string name, Building storedAt) : base(name, storedAt) {
        }
    }
}