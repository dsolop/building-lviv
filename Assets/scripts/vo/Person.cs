using System;
using System.Collections.Generic;

namespace game.scripts.vo {
    [Serializable]
    public class Person : BusinessUnit {
        public Sex sex { get; private set; }
        public byte age { get; private set; }
        public PersonNeeds needs { get; private set; }
        public PersonNeeds needsFulfillment { get; private set; }
        public PersonSkills skills { get; private set; }
        public PersonStats stats { get; private set; }
        public Job job { get; private set; }

        public Person(string name, Sex sex, byte age) : base(name) {
            this.age = age;
            this.sex = sex;
            this.needs = new PersonNeeds();
            this.needsFulfillment = new PersonNeeds();
        }
    }

    public enum Sex {
        M, F
    }
}