using System;

namespace game.scripts.vo {
    [Serializable]
    public class Product : Good {
        public Product(string name, Building storedAt) : base(name, storedAt) {
        }
    }
}