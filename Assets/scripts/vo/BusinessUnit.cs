using System;
using System.Collections.Generic;
using game.scripts.db;

namespace game.scripts.vo {

    [Serializable]
    public class BusinessUnit : GuidIdVo {
        
        public decimal money { get; private set; }
        public HashSet<Credit> credit { get; private set; }
        public Dictionary<Sellable, byte> assets { get; private set; }

        public BusinessUnit (string name) : base(name) {
            money = 0;
            credit = new HashSet<Credit>();
            assets = new Dictionary<Sellable, byte>();
        }
    }
}