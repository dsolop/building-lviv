using System;
using game.scripts.db;

namespace game.scripts.vo {
    [Serializable]
    public class Sellable : GuidIdVo {
        public Sellable(string name) : base(name) {
        }
    }
}