using System;

namespace game.scripts.vo {
    [Serializable]
    public enum OwnershipType {
        SHARED, SINGLE
    }
}