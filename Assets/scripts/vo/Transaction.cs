using System;
using System.Collections.Generic;
using game.scripts.db;

namespace game.scripts.vo {

    [Serializable]
    public class Transaction : GuidIdVo {

        public BusinessUnit seller { get; private set; }
        public BusinessUnit buyer { get; private set; }
        public List<Sellable> items { get; private set; }
        public decimal price { get; private set; }
        public double timestamp { get; private set; }
        public Transaction() : base() {
        }
        public Transaction(BusinessUnit seller, BusinessUnit buyer, List<Sellable> items, decimal price) : base() {
            this.seller = seller;
            this.buyer = buyer;
            this.items = items;
            this.price = price;
            this.timestamp = GameTime.instance.currentTimestep;
        }
        
        public override bool Equals(object obj) {
            if (obj == null || GetType() != obj.GetType()) {
                return false;
            }
            return ((BusinessUnit)obj).id.Equals(this.id);
        }
        
        public override int GetHashCode() {
            return id.GetHashCode();
        }
    }
}