using System;

namespace game.scripts.vo {

    [Serializable]
    public class Credit {
        public Bank borrower { get; private set; }
        public BusinessUnit lender { get; private set; }
        public decimal amount { get; private set; }
        public decimal interest { get; private set; }

        // todo recalc interest yearly
        public decimal dept { get; private set; }
        
        public Credit(Bank borrower, BusinessUnit lender, decimal amount, decimal interest) {
            this.borrower = borrower;
            this.lender = lender;
            this.amount = amount;
            this.interest = interest;
            dept = (1 + interest) * amount;
        }

        public decimal decreaseDept(decimal amount) {
            if (amount >= dept) {
                dept = 0;
            } else {
                dept -= amount;
            }
            return dept;
        }

    }
}