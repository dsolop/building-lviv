using System;
using game.scripts.vo.configured;
using UnityEngine;

namespace game.scripts.vo {
    [Serializable]
    public class Building : Sellable {
        public BuildingType buildingType { get; private set; }
        public Vector2Int coords { get; private set; }
        public ResourceRecipe recipe { get; set; }
        public OwnershipType ownershipType { get; private set; }
        public BusinessUnit productionOwner;
        public BusinessUnit houseOwner;
        
        public Building (string name, BuildingType buildingType, Vector2Int coords) : base(name) {
            this.buildingType = buildingType;
            this.coords = coords;
            ownershipType = OwnershipType.SINGLE;
            // TODO set owner
        }
    }
}