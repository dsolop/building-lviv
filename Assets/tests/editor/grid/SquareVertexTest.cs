﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using game.scripts.grid;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class SquareVertexTest {

        public static Vector2Int gridSize = new Vector2Int(5, 3);

        [Test]
        public void testGetNeighboursSE_Corner() {
            SquareVertex squareVertex = new SquareVertex(0, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(1, gridSize), neighbours);
            Assert.Contains(new SquareVertex(2, gridSize), neighbours);
            Assert.Contains(new SquareVertex(4, gridSize), neighbours);
            Assert.AreEqual(5, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighboursSE_Side() {
            SquareVertex squareVertex = new SquareVertex(15, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(10, gridSize), neighbours);
            Assert.Contains(new SquareVertex(14, gridSize), neighbours);
            Assert.Contains(new SquareVertex(16, gridSize), neighbours);
            Assert.Contains(new SquareVertex(17, gridSize), neighbours);
            Assert.Contains(new SquareVertex(19, gridSize), neighbours);
            Assert.AreEqual(3, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighboursSE_Middle() {
            SquareVertex squareVertex = new SquareVertex(70, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(44, gridSize), neighbours);
            Assert.Contains(new SquareVertex(45, gridSize), neighbours);
            Assert.Contains(new SquareVertex(49, gridSize), neighbours);
            Assert.Contains(new SquareVertex(65, gridSize), neighbours);
            Assert.Contains(new SquareVertex(69, gridSize), neighbours);
            Assert.Contains(new SquareVertex(71, gridSize), neighbours);
            Assert.Contains(new SquareVertex(72, gridSize), neighbours);
            Assert.Contains(new SquareVertex(74, gridSize), neighbours);
            Assert.AreEqual(0, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighboursSW_Corner() {
            SquareVertex squareVertex = new SquareVertex(21, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(20, gridSize), neighbours);
            Assert.Contains(new SquareVertex(24, gridSize), neighbours);
            Assert.Contains(new SquareVertex(23, gridSize), neighbours);
            Assert.AreEqual(5, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighboursSW_Side() {
            SquareVertex squareVertex = new SquareVertex(46, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(21, gridSize), neighbours);
            Assert.Contains(new SquareVertex(24, gridSize), neighbours);
            Assert.Contains(new SquareVertex(45, gridSize), neighbours);
            Assert.Contains(new SquareVertex(48, gridSize), neighbours);
            Assert.Contains(new SquareVertex(49, gridSize), neighbours);
            Assert.AreEqual(3, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighboursSW_Middle() {
            SquareVertex squareVertex = new SquareVertex(36, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(11, gridSize), neighbours);
            Assert.Contains(new SquareVertex(14, gridSize), neighbours);
            Assert.Contains(new SquareVertex(19, gridSize), neighbours);
            Assert.Contains(new SquareVertex(35, gridSize), neighbours);
            Assert.Contains(new SquareVertex(38, gridSize), neighbours);
            Assert.Contains(new SquareVertex(39, gridSize), neighbours);
            Assert.Contains(new SquareVertex(41, gridSize), neighbours);
            Assert.Contains(new SquareVertex(44, gridSize), neighbours);
            Assert.AreEqual(0, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighbourNE_Corner() {
            SquareVertex squareVertex = new SquareVertex(52, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(50, gridSize), neighbours);
            Assert.Contains(new SquareVertex(53, gridSize), neighbours);
            Assert.Contains(new SquareVertex(54, gridSize), neighbours);
            Assert.AreEqual(5, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighboursNE_Side() {
            SquareVertex squareVertex = new SquareVertex(2, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(0, gridSize), neighbours);
            Assert.Contains(new SquareVertex(3, gridSize), neighbours);
            Assert.Contains(new SquareVertex(4, gridSize), neighbours);
            Assert.Contains(new SquareVertex(27, gridSize), neighbours);
            Assert.Contains(new SquareVertex(29, gridSize), neighbours);
            Assert.AreEqual(3, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighboursNE_Middle() {
            SquareVertex squareVertex = new SquareVertex(32, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(27, gridSize), neighbours);
            Assert.Contains(new SquareVertex(29, gridSize), neighbours);
            Assert.Contains(new SquareVertex(30, gridSize), neighbours);
            Assert.Contains(new SquareVertex(33, gridSize), neighbours);
            Assert.Contains(new SquareVertex(34, gridSize), neighbours);
            Assert.Contains(new SquareVertex(54, gridSize), neighbours);
            Assert.Contains(new SquareVertex(57, gridSize), neighbours);
            Assert.Contains(new SquareVertex(59, gridSize), neighbours);
            Assert.AreEqual(0, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighbourNW_Corner() {
            SquareVertex squareVertex = new SquareVertex(73, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(71, gridSize), neighbours);
            Assert.Contains(new SquareVertex(72, gridSize), neighbours);
            Assert.Contains(new SquareVertex(74, gridSize), neighbours);
            Assert.AreEqual(5, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighboursNW_Side() {
            SquareVertex squareVertex = new SquareVertex(48, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(46, gridSize), neighbours);
            Assert.Contains(new SquareVertex(47, gridSize), neighbours);
            Assert.Contains(new SquareVertex(49, gridSize), neighbours);
            Assert.Contains(new SquareVertex(73, gridSize), neighbours);
            Assert.Contains(new SquareVertex(74, gridSize), neighbours);
            Assert.AreEqual(3, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighboursNW_Middle() {
            SquareVertex squareVertex = new SquareVertex(8, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(8, neighbours.Length);
            Assert.Contains(new SquareVertex(6, gridSize), neighbours);
            Assert.Contains(new SquareVertex(7, gridSize), neighbours);
            Assert.Contains(new SquareVertex(9, gridSize), neighbours);
            Assert.Contains(new SquareVertex(13, gridSize), neighbours);
            Assert.Contains(new SquareVertex(14, gridSize), neighbours);
            Assert.Contains(new SquareVertex(33, gridSize), neighbours);
            Assert.Contains(new SquareVertex(34, gridSize), neighbours);
            Assert.Contains(new SquareVertex(39, gridSize), neighbours);
            Assert.AreEqual(0, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetNeighboursCenter() {
            SquareVertex squareVertex = new SquareVertex(9, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getNeighbours();

            Assert.AreEqual(4, neighbours.Length);
            Assert.Contains(new SquareVertex(5, gridSize), neighbours);
            Assert.Contains(new SquareVertex(6, gridSize), neighbours);
            Assert.Contains(new SquareVertex(7, gridSize), neighbours);
            Assert.Contains(new SquareVertex(8, gridSize), neighbours);
            Assert.AreEqual(0, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursSE_Corner() {
            SquareVertex squareVertex = new SquareVertex(0, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.AreEqual(3, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursSE_Side() {
            SquareVertex squareVertex = new SquareVertex(50, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(27, gridSize), neighbours);
            Assert.AreEqual(2, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursSE_Bottom() {
            SquareVertex squareVertex = new SquareVertex(10, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(6, gridSize), neighbours);
            Assert.AreEqual(2, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursSE_Middle() {
            SquareVertex squareVertex = new SquareVertex(35, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(8, gridSize), neighbours);
            Assert.Contains(new SquareVertex(12, gridSize), neighbours);
            Assert.Contains(new SquareVertex(31, gridSize), neighbours);
            Assert.AreEqual(0, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursSW_Corner() {
            SquareVertex squareVertex = new SquareVertex(21, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.AreEqual(3, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursSW_Side() {
            SquareVertex squareVertex = new SquareVertex(71, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(48, gridSize), neighbours);
            Assert.AreEqual(2, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursSW_Bottom() {
            SquareVertex squareVertex = new SquareVertex(11, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(15, gridSize), neighbours);
            Assert.AreEqual(2, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursSW_Middle() {
            SquareVertex squareVertex = new SquareVertex(41, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(18, gridSize), neighbours);
            Assert.Contains(new SquareVertex(22, gridSize), neighbours);
            Assert.Contains(new SquareVertex(45, gridSize), neighbours);
            Assert.AreEqual(0, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursNE_Corner() {
            SquareVertex squareVertex = new SquareVertex(52, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.AreEqual(3, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursNE_Side() {
            SquareVertex squareVertex = new SquareVertex(23, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(46, gridSize), neighbours);
            Assert.AreEqual(2, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursNE_Top() {
            SquareVertex squareVertex = new SquareVertex(57, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(53, gridSize), neighbours);
            Assert.AreEqual(2, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursNE_Middle() {
            SquareVertex squareVertex = new SquareVertex(7, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(3, gridSize), neighbours);
            Assert.Contains(new SquareVertex(26, gridSize), neighbours);
            Assert.Contains(new SquareVertex(30, gridSize), neighbours);
            Assert.AreEqual(0, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursNW_Corner() {
            SquareVertex squareVertex = new SquareVertex(73, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.AreEqual(3, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursNW_Side() {
            SquareVertex squareVertex = new SquareVertex(48, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(71, gridSize), neighbours);
            Assert.AreEqual(2, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursNW_Top() {
            SquareVertex squareVertex = new SquareVertex(63, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(67, gridSize), neighbours);
            Assert.AreEqual(2, getElementCount(neighbours, null));
        }

        [Test]
        public void testGetSameCoordsNeighboursNW_Middle() {
            SquareVertex squareVertex = new SquareVertex(33, gridSize);
            
            SquareVertex[] neighbours = squareVertex.getSameCoordsNeighbours();

            Assert.AreEqual(3, neighbours.Length);
            Assert.Contains(new SquareVertex(37, gridSize), neighbours);
            Assert.Contains(new SquareVertex(56, gridSize), neighbours);
            Assert.Contains(new SquareVertex(60, gridSize), neighbours);
            Assert.AreEqual(0, getElementCount(neighbours, null));
        }

        public int getElementCount(SquareVertex[] array, SquareVertex element) {
            return array.Where(squareVertex => Object.Equals(squareVertex, element)).Count(); 
        }
    }
}
