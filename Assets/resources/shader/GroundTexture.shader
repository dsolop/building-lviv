﻿Shader "Custom/GroundTexture"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _GrassTex ("Albedo (RGB)", 2D) = "white" {}
        _StoneTex ("Albedo (RGB)", 2D) = "white" {}
        // _MyArr ("Tex", 2DArray) = "" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _GrassTex;
        sampler2D _StoneTex;

        struct Input
        {
            float2 uv_GrassTex;
            float2 uv_StoneTex;
            float2 uv;
            UNITY_FOG_COORDS(1)
            float4 vertex;
            float3 worldPos;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        // fixed4 frag (Input IN) : SV_Target
        // {
        //     // sample the texture
        //     fixed4 col = tex2D(_GrassTex, IN.uv);
        //     // apply fog
        //     // UNITY_APPLY_FOG(IN.fogCoord, col);
        //     return col;
        // }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // if (IN.vertex.y < 2) {
            //     // Albedo comes from a texture tinted by color
            //     fixed4 c = tex2D (_StoneTex, IN.uv_StoneTex) * _Color;
            //     o.Albedo = c.rgb;
            //     o.Alpha = c.a;
            // } else {
            //     // Albedo comes from a texture tinted by color
            // print(IN.vertex);
            fixed4 c;
            if (IN.worldPos.y < 1.8f) {
                c = tex2D(_GrassTex, IN.uv_GrassTex); 
            } else if (IN.worldPos.y < 2.0f) {
                c = lerp(tex2D(_GrassTex, IN.uv_GrassTex), tex2D(_StoneTex, IN.uv_StoneTex), (IN.worldPos.y - 1.8f) * 5)* _Color;
            } else {
                c = tex2D(_StoneTex, IN.uv_StoneTex); 
            }
            o.Albedo = c.rgb;
            o.Alpha = c.a;
            // }
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
